package com.craftinginjava.match;

public interface Action {

    void perform();

}
