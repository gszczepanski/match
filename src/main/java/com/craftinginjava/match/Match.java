package com.craftinginjava.match;


import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.*;

import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class Match {

    private final Set<Object> casesSet = new HashSet<>();

    private Case tailCase;
    private Case headCase;
    private Action defaultAction;

    public static Match match() {
        return new Match();
    }

    public Match _case(Object value, Action action) {
        validateValueCase(value, action);

        casesSet.add(value);
        addCase(Case.valueCase(value, action));
        return this;
    }
    private void validateValueCase(Object value, Action action) {
        if (casesSet.contains(value)) {
            throw new RuntimeException(format("Case for value (%s) is already defined", value));
        }
        validateAction(action);
    }

    private void addCase(Case _case) {
        if (tailCase == null) {
            tailCase = _case;
            headCase = _case;
        } else {
            headCase.next = _case;
            headCase = _case;
        }
    }

    public Match _case(Function function, Action action) {
        validateFunctionCase(function, action);

        casesSet.add(function);
        addCase(Case.functionCase(function, action));
        return this;
    }

    private void validateFunctionCase(Function function, Action action) {
        if (function == null) {
            throw new NullPointerException("Function can not be null!");
        }

        if (casesSet.contains(function)) {
            throw new RuntimeException(format("Case for function (%s) is already defined", function));
        }
        validateAction(action);
    }

    private void validateAction(Action action) {
        if (action == null) {
            throw new NullPointerException("Action must be defined!");
        }
    }

    public Match _default(Action action) {
        if (defaultAction != null) {
            throw new RuntimeException("Default case is already defined. Only one default case allowed");
        }

        validateAction(action);
        defaultAction = action;

        return this;
    }

    public Match _null(Action action) {
        return _case((Object) null, action);
    }

    public void _for(Object value) {
        Action action = findActionForValue(value);
        action.perform();
    }

    public void _for(Function function) {
        if (function == null) {
            _forNull();
            return;
        }
        _for(function._do());
    }

    private void _forNull() {
        Action action = findActionForValue(null);
        action.perform();
    }

    private Action findActionForValue(Object value) {
        Action actionForValue = tailCase.action(value);

        if (actionForValue == null) {
            actionForValue = defaultAction;
        }

        if (actionForValue == null) {
            throw new RuntimeException(String.format("Could not find case for value %s. If value is not handled by _case, then use _default " +
                    "to define default values handling"));
        }
        return actionForValue;
    }

    @RequiredArgsConstructor
    private static class Case {

        private final Object value;
        private final Function function;
        private final Action action;

        private Case next;

        private static Case valueCase(Object value, Action action) {
            return new Case(value, null, action);
        }

        private static Case functionCase(Function function, Action action) {
            return new Case(null, function, action);
        }

        Action action(Object value) {
            return isActionFor(value) ? action : dispatchToNext(value);
        }

        private boolean isActionFor(Object value) {
            return function != null && functionResultMatch(value, function._do()) || Objects.equals(this.value, value);
        }

        private boolean functionResultMatch(Object value, Object functionResult) {
            return functionResult != null && functionResult.equals(value) || functionResult == null && value == null;
        }

        private Action dispatchToNext(Object value) {
            return next == null ? null : next.action(value);
        }
    }

}
