package com.craftinginjava.match;

public interface Function {

    Object _do();

}
