package com.craftinginjava.match;

import org.junit.Test;

import static com.craftinginjava.match.Match.match;
import static org.assertj.core.api.Assertions.*;

public class MatchTest {

    private String result;

    @Test
    public void shouldIntegerValueMatchProduceDefaultCaseResult() {
        int value = 123;

        match()
            ._case(1, () -> result = "First case")
            ._case(2, () -> result = "Second case")
            ._case("Hello World", () -> result = "Hello world case")
            ._case(() -> Math.random(), () -> result = "Random stuff")
            ._default(() -> result = "Undefined :<")
            ._for(value);

        assertThat(result).isEqualTo("Undefined :<");
    }

    @Test
    public void shouldIntegerValueMatchCaseResult() {
        int value = 2;

        match()
            ._case(1, () -> result = "First case")
            ._case(2, () -> result = "Second case")
            ._case("Hello World", () -> result = "Hello world case")
            ._case(() -> Math.random(), () -> result = "Random stuff")
            ._default(() -> result = "Undefined :<")
            ._for(value);

        assertThat(result).isEqualTo("Second case");
    }

    @Test
    public void shouldStringValueMatchCaseResult() {
        String value = "Hello World";

        match()
            ._case(1, () -> result = "First case")
            ._case(2, () -> result = "Second case")
            ._case("Hello World", () -> result = "Hello world case")
            ._case(() -> Math.random(), () -> result = "Random stuff")
            ._default(() -> result = "Undefined :<")
            ._for(value);

        assertThat(result).isEqualTo("Hello world case");
    }

    @Test
    public void shouldNullValueMatchNullCaseResult() {
        match()
            ._case(1, () -> result = "First case")
            ._case(2, () -> result = "Second case")
            ._null(() -> result = "NULL case")
            ._default(() -> result = "Undefined :<")
            ._for(null);

        assertThat(result).isEqualTo("NULL case");
    }

    @Test
    public void shouldFunctionMatchProduceSecondCaseResult() {
        Function f = () -> 970;

        match()
            ._case(960, () -> result = "First case")
            ._case(970, () -> result = "Second case")
            ._for(f);

        assertThat(result).isEqualTo("Second case");
    }

    @Test
    public void shouldTriggerFirstCaseInChainForGivenFunctionResult() {
        int value = 2;

        match()
            ._case(1, () -> result = "First case")
            ._case(() -> 2, () -> result = "This will be triggered")
            ._case(() -> 2, () -> result = "Even if the function result is the same as above, this won't be triggered")
            ._for(value);

        assertThat(result).isEqualTo("This will be triggered");
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenCaseForValueIsDuplicated() {
        int value = 2;

        match()
            ._case(1, () -> result = "First case")
            ._case(2, () -> result = "Second case")
            ._case(2, () -> result = "Duplicated case that throws exception")
            ._for(value);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenNullCaseIsDuplicated() {
        int value = 2;

        match()
            ._null(() -> result = "Null case")
            ._null(() -> result = "Duplicated Null case")
            ._for(value);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenDefaultCaseIsDuplicated() {
        int value = 2;

        match()
            ._default(() -> result = "Default case")
            ._default(() -> result = "Duplicated default case")
            ._for(value);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenActionPassedToValueCaseIsNull() {
        match()
            ._case(1, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenActionPassedToFunctionCaseIsNull() {
        match()
            ._case(() -> 1, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenActionPassedToNullCaseIsNull() {
        match()
            ._null(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenActionPassedToDefaultCaseIsNull() {
        match()
            ._default(null);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenThereIsNoCaseDefined() {
        int value = 2;

        match()
            ._for(value);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenThereIsNoDefaultCaseAndValueDoesNotMatchAnyOtherCase() {
        int value = 3;

        match()
            ._case(1, () -> result = "First case")
            ._case(2, () -> result = "Second case")
            ._for(value);
    }


}