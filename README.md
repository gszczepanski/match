# Match
Simple tool for defining Chain of Responsibility pettern in Java to replace huge if/else or switch conditions.
Inspired by match instrucion from Scala.

# Usage

```
Match.match()
            ._case(1, () -> result = "First case") // Define case for value 1
            ._case(2, () -> result = "Second case")
            ._case("Hello World", () -> result = "Hello world case") // Define case fo String
            ._case(() -> Math.random(), () -> result = "Random stuff") // Define case for function 
            ._null(() -> result = "Null case") // Define case for null
            ._default(() -> result = "Undefined :<") // Define default case
            ._for(value); // Invoke chain for value
```

